package com.skybox.gdd;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ReadControlData {
	
	public static boolean processInitializationFile(String bu) throws FileNotFoundException{
		boolean result = true;
		Logger LOG = UtilityLogger.LOGGER;
		String file = "C://RemoteCRMData/RemoteCRMData" + bu + "A.ini";
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
		    	   if(!line.contentEquals("")){
		    		   if(!line.substring(0,1).contentEquals(";")){
			    		   // line has valid data
		    			   String dataType = line.substring(0,line.indexOf("="));
		    			   String data = line.substring(line.indexOf("=") + 1);
		    			   switch (dataType) {
		    			   case "BUNumber":
		    				   BU_Data.buNumber = data;
		    				   break;
		    			   case "DB_URL":
		    				   BU_Data.DbURL = data;
		    				   break;
		    			   case "DB_UserId":
		    				   BU_Data.DbId = data;
		    				   break;
		    			   case "DB_UserPw":
		    				   BU_Data.DbPw = data;
		    				   break;
		    			   case "Log_File_Lvl":
		    				   	BU_Data.LogLevel = DoLogLevel(data);
			    				break;
		    			   case "RunPrgm":
		    				   if(!data.contains("TRUE")){
		    					   result = false;
		    				   }else{
		    					   result = true;
		    				   }
		    				   break;
		    			   case "PollTimer":
		    				   BU_Data.pollTimer = Integer.valueOf(data);
		    				   break;
		    			   case "LocalTimezone":
		    				   BU_Data.LocalTimeZone = data;
		    				   break;
		    			   case "ftpServer":
		    				   BU_Data.ftpServer = data;
		    				   break;
		    			   case "ftpPort":
		    				   BU_Data.ftpPort = data;
		    				   break;
		    			   case "ftpUser":
		    				   BU_Data.ftpUser = data;
		    				   break;
		    			   case "ftpPass":
		    				   BU_Data.ftpPass = data;
		    				   break;
		    			   case "ftpPassive":
		    				   BU_Data.passive = false;
		    				   if(data.contentEquals("TRUE")){
		    					   BU_Data.passive = true;
		    				   }else{
		    					   BU_Data.passive = false;
		    				   }
		    				   break;
		    			   case "emailAddress":
		    				   BU_Data.emailAddress = data;
		    				   break;
		    			   case "SFTP":
		    				   if(data.contentEquals("TRUE")){
		    					   BU_Data.sftp = true;
		    				   }else{
		    					   BU_Data.sftp = false;
		    				   }
		    				   break;
		    			   case "UploadTable":
		    				   BU_Data.uploadTable = data;
		    				   break;
		    			   case "UploadDir":
		    				   BU_Data.uploadDir = data;
		    				   break;
		    			   case "DownloadDir":
		    				   BU_Data.downLoadDir = data;
		    				   break;
		    			   case "filePrefix":
		    				   BU_Data.filePrefix = data;
		    				   break;
		    			   default:
		    				   break;
		    			   }
			    	   }
			       }
			}
			br.close();
		} catch (IOException e) {
			result = false;
			LOG.log(Level.SEVERE,"I/O Exception",e);
			e.printStackTrace();
		}
		if(result){
			String ServiceIni = "C://RemoteCRMData/" + bu + ".ini";
			File f = new File(ServiceIni);
			if(f.exists() && f.isFile()) {
				try {
					BufferedReader br = new BufferedReader(new FileReader(ServiceIni));
					String line = br.readLine();
					if(line.contentEquals("1")){
						result = false;
					}
					br.close();
				} catch (FileNotFoundException e) {
					result = false;
					LOG.log(Level.SEVERE,"FileNotFound Exception",e);
					e.printStackTrace();
				} catch (IOException e) {
					result = false;
					LOG.log(Level.SEVERE,"I/O Exception",e);
					e.printStackTrace();
				}
			}
		}
		
		return result;
	}
	private static Level DoLogLevel(String lvl){
		Level rtnLevel = null;
		switch(lvl){
		case "ALL":
			rtnLevel = Level.ALL;
			break;
		case "SEVERE":
			rtnLevel = Level.SEVERE;
			break;
		case "WARNING":
			rtnLevel = Level.WARNING;
			break;
		case "INFO":
			rtnLevel = Level.INFO;
			break;
		case "CONFIG":
			rtnLevel = Level.CONFIG;
			break;
		case "FINE":
			rtnLevel = Level.FINE;
			break;
		case "FINER":
			rtnLevel = Level.FINER;
			break;
		case "FINEST":
			rtnLevel = Level.FINEST;
			break;
		case "NONE":
			rtnLevel = Level.OFF;
			break;
		}
		return rtnLevel;
	}
	public static String decript(String str){
		String decriptStr = "";
		Integer[] keys = new Integer[12];
		String keyStr = "SKYBOX";
		char[] keyStrArray = keyStr.toCharArray();
		// create offsets based on KEY String
		for (int i=0;i<keyStrArray.length;i++){
			Integer val = (int) keyStrArray[i];
			keys[i*2] = Math.floorDiv(val, 10);
			keys[i*2 + 1] = val - (keys[i*2] * 10);
		}
		// have offsets
		Integer x = 0; // pointer for key offsets
		char[] strArray = str.toCharArray();
		for (int i=0;i<strArray.length;i++){
			Integer val = (int) strArray[i] + keys[x];
			decriptStr += String.valueOf(Character.toChars(val));
			x++;
			if(x == keys.length) x = 0;
		}
		return decriptStr;
	}
}
