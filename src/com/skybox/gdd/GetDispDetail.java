package com.skybox.gdd;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.SocketException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

@Path("/GetDispDetail")

public class GetDispDetail {
	
	public static boolean debug = false; //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	static Logger LOG;

	@Path("{BU}/{GetDate}")
	@GET
//	@Produces(MediaType.TEXT_HTML)
	@Produces("application/json")
//	public String processDisp(@PathParam("BU") String BU, @PathParam("GetDate") String getDateStr){
	public Response processDisp(@PathParam("BU") String BU, @PathParam("GetDate") String getDateStr){
		
		String response = "{'result':'Successful'}";
		// set up logging 
		UtilityLogger.initLogger("DispDetail");
		LOG = UtilityLogger.LOGGER;
		ResultSet rs = null;
		Statement stmt = null;		
		Connection conn = null;
		//getDateStr = getDateStr.replaceAll("-", "/");  // just in case
		try {
			// get data associated with BU
			ReadControlData.processInitializationFile(BU);
			// create select statement for DB
			OutboundMapping outMap = new OutboundMapping(BU);
			String fields = "";
			for (int x=0;x<outMap.OutboundMap.length;x++){
				if(x==0){
					fields += outMap.OutboundMap[x].ColumnName;
				}else{
					fields += ", " + outMap.OutboundMap[x].ColumnName;
				}
			}
			
			for (int s=0;s<2;s++){
				String sql = "";
				switch(BU){
				case "4594410":
					if (s==0){
						sql = "SELECT " + fields + " FROM CallHistory WHERE pDate =  '" + getDateStr + "' and SkillID not in (select SkillID from ThirdPartySkills) ORDER BY ContactID ;";
						//sql = "SELECT " + fields + " FROM CallHistory WHERE DispositionCode in ('81','82','55','56','57') and SkillID not in (select SkillID from ThirdPartySkills) ORDER BY ContactID ;";
					}else{
						sql = "SELECT " + fields + " FROM CallHistory WHERE pDate =  '" + getDateStr + "' and SkillID in (select SkillID from ThirdPartySkills) ORDER BY ContactID ;";
						//sql = "SELECT " + fields + " FROM CallHistory WHERE DispositionCode in ('81','82','55','56','57') and SkillID in (select SkillID from ThirdPartySkills) ORDER BY ContactID ;";
					}
					break;
//				case "4595490":
//					sql = "SELECT " + fields + " FROM Call_History WHERE Date =  '" + getDateStr + "' ORDER BY Time ;";
//					break;
				}
				conn = ConnectionFactory.getConnection();
				
				stmt = conn.createStatement();
				rs = stmt.executeQuery(sql);
				Integer count = 0;
				String fileName = "";
				switch (BU){
//				case "4595490":
//					fileName = getDateStr.replaceAll("-","") + "_RS_Results.csv";
//					break;
				case "4594410":
					if (s==0){
						fileName = BU_Data.filePrefix + getDateStr.replaceAll("-","") + ".csv";
						//fileName = BU_Data.filePrefix + "Update020819" + ".csv";
					}else{
						fileName = BU_Data.filePrefix + getDateStr.replaceAll("-","") + "-3rd.csv";
						//fileName = BU_Data.filePrefix + "Update020819" + "-3rd.csv";
					}
					break;
				}
				String path = "C:/RemoteCRMData/";
				File testFile = new File(path + fileName);
				if(testFile.exists()){
					testFile.delete();
				}
				PrintWriter csvfile = new PrintWriter(path + fileName);
				
				while(rs.next()){
					count++;
					String line = "";
					for(int i=1;i<outMap.OutboundMap.length + 1;i++){
						if(i==1){
							line += outMap.convert(rs.getString(i),i-1);
						}else{
							line += "," + outMap.convert(rs.getString(i),i-1);
						}
					}
				    csvfile.println(line);
				}
				csvfile.close();
	
				System.out.println("Total records: " + count);
				LOG.info("Total records: " + count);
				
				if (!debug) {

					if(BU_Data.sftp){
						doSFTPUpload(path, fileName);
					}else{
						doFTPUpload(path, fileName);
					}
					LOG.info("Completed (S)FTP upload");
					// delete file
					if(testFile.exists()){
						testFile.delete();
						LOG.info("Deleted Upload local file");
					}
				}
			}
			// Do cleanup of RUI data (delete any data older than 31 days
			String delStmt1 = "delete from CallHistory where pDate < Date_Add(now(), interval -31 day);";
			String delStmt2 = "delete from CallList where Dialer_Call_Date < Date_Add(now(), interval -31 day);";
			String delStmt3 = "delete from Customer_Phone where AccountNum NOT IN (Select AccountNum from CallList);";
			
			stmt.execute(delStmt1);
			stmt.execute(delStmt2);
			stmt.execute(delStmt3);
			LOG.info("Deleted Old Records");
//			UtilityLogger.sendEmail("Disposition Data for " + getDateStr, path + fileName);
		} catch (SQLException e) {
			response = "{'result':'problem - sql Access'}";
			LOG.log(Level.SEVERE,"SQL Exception",e);
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			LOG.log(Level.SEVERE,"No ini file found",e);
			response = "{'result':'problem - no ini file'}";
			e.printStackTrace();
		} catch (ParseException e) {
			LOG.log(Level.SEVERE,"Parsing Exception",e);
			response = "{'result':'problem - parsing error'}";
			e.printStackTrace();
		} catch (NumberFormatException e) {
			response = "{'result':'problem - number format error'}";
			e.printStackTrace();
		} catch (SocketException e) {
			LOG.log(Level.SEVERE,"Socket Exception",e);
			response = "{'result':'problem - socket error'}";
			e.printStackTrace();
		} catch (IOException e) {
			LOG.log(Level.SEVERE,"IO Exception",e);
			response = "{'result':'problem - IO Exception'}";
			e.printStackTrace();
		} catch (SftpException e) {
			LOG.log(Level.SEVERE,"SFTP Exception",e);
			response = "{'result':'problem - SFTP Exception'}";
			e.printStackTrace();
		} catch (JSchException e) {
			LOG.log(Level.SEVERE,"JSch Exception",e);
			response = "{'result':'problem - JSch Exception'}";
			e.printStackTrace();
		}finally{
			DbUtil.close(rs);
			rs = null;
			DbUtil.close(stmt);
			stmt = null;
			DbUtil.close(conn);
			conn = null;
			for (Handler h : LOG.getHandlers()){
				LOG.removeHandler(h);
				h.close();
			}
			LOG = null;
		}
		return Response.status(200).entity(response).build();
//		return response;
	}
//	@Path("{BU}/{GetDate}/{table}")
//	@GET
////	@Produces(MediaType.TEXT_HTML)
//	@Produces("application/json")
////	public String processDisp(@PathParam("BU") String BU, @PathParam("GetDate") String getDateStr){
//	public Response processDisp2(@PathParam("BU") String BU, @PathParam("GetDate") String getDateStr, @PathParam("table") String table){
//		
//		String response = "{'result':'Successful'}";
//		// set up logging 
//		UtilityLogger.initLogger("DispDetail");
//		LOG = UtilityLogger.LOGGER;
//		ResultSet rs = null;
//		Statement stmt = null;		
//		Connection conn = null;
//		getDateStr = getDateStr.replaceAll("-", "/");  // just in case
//		try {
//			// get data associated with BU
//			ReadControlData.processInitializationFile(BU);
//			// create select statement for DB
//			OutboundMapping outMap = new OutboundMapping(BU);
//			String fields = "";
//			for (int x=0;x<outMap.OutboundMap.length;x++){
//				if(x==0){
//					fields += outMap.OutboundMap[x].ColumnName;
//				}else{
//					fields += ", " + outMap.OutboundMap[x].ColumnName;
//				}
//			}
//			String sql = "";
//			switch(BU){
//			case "4594410":
//				sql = "SELECT " + fields + " FROM CallHistory WHERE pDate =  '" + getDateStr + "' ORDER BY ContactID ;";
//				break;
//			case "4595490":
//				sql = "SELECT " + fields + " FROM " + table + " WHERE Date =  '" + getDateStr + "' ORDER BY inContact_Id ;";
//				break;
//			}
//			conn = ConnectionFactory.getConnection();
//			
//			stmt = conn.createStatement();
//			rs = stmt.executeQuery(sql);
//			Integer count = 0;
//			String[] modDate = getDateStr.split("/");
//			if(modDate[0].length() == 1) modDate[0] = "0" + modDate[0];
//			if(modDate[1].length() == 1) modDate[1] = "0" + modDate[1];
//			getDateStr = modDate[2] + modDate[0] + modDate[1];
//			String fileName = "";
//			switch (BU){
//			case "4595490":
//				if(table.contains("_RS")){
//					fileName = getDateStr + "_RS_Results.csv";
//				}else{
//					fileName = getDateStr + "_DENO_Results.csv";
//				}
//				break;
//			case "4594410":
//				fileName = BU_Data.filePrefix + getDateStr + ".csv";
//				break;
//			}
//			String path = "C:/RemoteCRMData/";
//			File testFile = new File(path + fileName);
//			if(testFile.exists()){
//				testFile.delete();
//			}
//			PrintWriter csvfile = new PrintWriter(path + fileName);
//			
//			
//			if(BU.contentEquals("4595490")){
//				csvfile.println(fields);
//			}
//			
//			
//			while(rs.next()){
//				count++;
//				String line = "";
//				for(int i=1;i<outMap.OutboundMap.length + 1;i++){
//					if(i==1){
//						line += outMap.convert(rs.getString(i),i-1);
//					}else{
//						line += "," + outMap.convert(rs.getString(i),i-1);
//					}
//				}
//			    csvfile.println(line);
//			}
//			csvfile.close();
//
//			System.out.println("Total records: " + count);
//			LOG.info("Total records: " + count);
//			
//			if(BU_Data.sftp){
//				doSFTPUpload(path, fileName);
//			}else{
//				doFTPUpload(path, fileName);
//			}
//			// delete file
////			if(testFile.exists()){
////				testFile.delete();
////			}
//
////			UtilityLogger.sendEmail("Disposition Data for " + getDateStr, path + fileName);
//		} catch (SQLException e) {
//			response = "{'result':'problem - sql Access'}";
//			LOG.log(Level.SEVERE,"SQL Exception",e);
//			e.printStackTrace();
//		} catch (FileNotFoundException e) {
//			LOG.log(Level.SEVERE,"No ini file found",e);
//			response = "{'result':'problem - no ini file'}";
//			e.printStackTrace();
//		} catch (ParseException e) {
//			LOG.log(Level.SEVERE,"Parsing Exception",e);
//			response = "{'result':'problem - parsing error'}";
//			e.printStackTrace();
//		} catch (NumberFormatException e) {
//			response = "{'result':'problem - number format error'}";
//			e.printStackTrace();
//		} catch (SocketException e) {
//			LOG.log(Level.SEVERE,"Socket Exception",e);
//			response = "{'result':'problem - socket error'}";
//			e.printStackTrace();
//		} catch (IOException e) {
//			LOG.log(Level.SEVERE,"IO Exception",e);
//			response = "{'result':'problem - IO Exception'}";
//			e.printStackTrace();
//		} catch (SftpException e) {
//			LOG.log(Level.SEVERE,"SFTP Exception",e);
//			response = "{'result':'problem - SFTP Exception'}";
//			e.printStackTrace();
//		} catch (JSchException e) {
//			LOG.log(Level.SEVERE,"JSch Exception",e);
//			response = "{'result':'problem - JSch Exception'}";
//			e.printStackTrace();
//		}finally{
//			DbUtil.close(rs);
//			rs = null;
//			DbUtil.close(stmt);
//			stmt = null;
//			DbUtil.close(conn);
//			conn = null;
//			for (Handler h : LOG.getHandlers()){
//				LOG.removeHandler(h);
//				h.close();
//			}
//			LOG = null;
//		}
//		return Response.status(200).entity(response).build();
////		return response;
//	}
//	
	private void doFTPUpload(String path, String fileName) throws NumberFormatException, SocketException, IOException{
		FTPClient ftpClient = new FTPClient();
		ftpClient.connect(BU_Data.ftpServer, Integer.parseInt(BU_Data.ftpPort));
		int replyCode = ftpClient.getReplyCode();
		if (FTPReply.isPositiveCompletion(replyCode)) {
		    boolean success = ftpClient.login(BU_Data.ftpUser, BU_Data.ftpPass);
		    if (success) {
		    	ftpClient.enterLocalPassiveMode();
		    	ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
		    	File sendFile = new File(path + fileName);
		    	String ftpPath = BU_Data.uploadDir;
		    	InputStream is = new FileInputStream(sendFile);
		    	boolean done = ftpClient.storeFile(ftpPath + fileName, is);
		    	is.close();
		    	if(done){
		    		sendFile.delete();
		    	}else{
		    		LOG.log(Level.SEVERE, "Booking file did not send to FTP server");
		    	}
		    }else{
	    		LOG.log(Level.SEVERE, "Could not log into FTP server");
		    }
		}else{
    		LOG.log(Level.SEVERE, "Could not connect to FTP server");
		}
        if(ftpClient.isConnected()){
        	ftpClient.logout();
        	ftpClient.disconnect();
        	LOG.info(fileName + " uploaded");
        }

	}
	private void doSFTPUpload(String path, String fileName) throws SftpException, JSchException, IOException {
	    Session session = null;
	    Channel channel = null;
	    ChannelSftp channelSftp = null;
	    FileInputStream fis = null;
        String host = BU_Data.ftpServer;
        String user = BU_Data.ftpUser;
        String pw = BU_Data.ftpPass;
	    try {
	        JSch jsch = new JSch();
	        session = jsch.getSession(user, host, 22);
	        session.setPassword(pw);
	        java.util.Properties config = new java.util.Properties();
	        config.put("StrictHostKeyChecking", "no");
	        session.setConfig(config);
	        session.connect();
	        channel = session.openChannel("sftp");
	        channel.connect();
	        channelSftp = (ChannelSftp) channel;
	        channelSftp.cd(BU_Data.uploadDir);
	        
	        File f1 = new File(path + fileName);
	        fis = new FileInputStream(f1);
	        channelSftp.put(fis, fileName, ChannelSftp.OVERWRITE);
	        if(session != null){
	            System.out.println("file Uploaded");
	            LOG.info(fileName + " uploaded");
	        }
	        fis.close();
	    }finally{
	    	
	    	if(channelSftp != null){
	            channelSftp.exit();
                session.disconnect();
                channelSftp = null;
               
	    	}
	    }

	}
	
}
