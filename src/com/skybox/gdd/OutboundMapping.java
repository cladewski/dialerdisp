package com.skybox.gdd;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class OutboundMapping {
	
	public Map[] OutboundMap;

	public OutboundMapping(String BU2use) {
		
	
		switch(BU2use){
		case "4594410":
		
			OutboundMap = new Map[12];
			for(int x=0;x<OutboundMap.length;x++){
				OutboundMap[x] = new Map();
			}
			Integer i = 0;
			OutboundMap[i].ColumnNumber = i;      //A
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "icMasterID";
			
			OutboundMap[i].ColumnNumber = i;      //B
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "ContactID";
				
			OutboundMap[i].ColumnNumber = i;       //C
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "PhoneID";
			
			OutboundMap[i].ColumnNumber = i;      //D
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "ResultCode";
			
			OutboundMap[i].ColumnNumber = i;      //E
			OutboundMap[i].ColumnType = "t2";
			OutboundMap[i++].ColumnName = "CallStartDateTime";
			
			OutboundMap[i].ColumnNumber = i;      // F
			OutboundMap[i].ColumnType = "t2";
			OutboundMap[i++].ColumnName = "CallEndDateTime";
			
			OutboundMap[i].ColumnNumber = i;      //G
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "CollectorID";
			
			OutboundMap[i].ColumnNumber = i;      //H
			OutboundMap[i].ColumnType = "t2";
			OutboundMap[i++].ColumnName = "DateStamp";
			
			OutboundMap[i].ColumnNumber = i;      //I
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "SkillID";
			
			OutboundMap[i].ColumnNumber = i;      //J
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "CCode";
			
			OutboundMap[i].ColumnNumber = i;      //K
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "CallNotes";
			
			OutboundMap[i].ColumnNumber = i;      //L
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "Inbound";
			break;
		case "4595490":
			OutboundMap = new Map[10];
			for(int x=0;x<OutboundMap.length;x++){
				OutboundMap[x] = new Map();
			}
			i = 0;
			OutboundMap[i].ColumnNumber = i;      //A
			OutboundMap[i].ColumnType = "i";
			OutboundMap[i++].ColumnName = "CUBS_Account_No";
			
			OutboundMap[i].ColumnNumber = i;      //B
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "Client_No";
				
			OutboundMap[i].ColumnNumber = i;       //C
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "Action_Code";
			
			OutboundMap[i].ColumnNumber = i;      //H
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "Disposition_Description";
			
			OutboundMap[i].ColumnNumber = i;      //D
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "Date";
			
			OutboundMap[i].ColumnNumber = i;      //E
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "Time";
			
			OutboundMap[i].ColumnNumber = i;      //H
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "Notes";
			
			OutboundMap[i].ColumnNumber = i;      //H
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "PhoneNumber";
			
			OutboundMap[i].ColumnNumber = i;      //G
			OutboundMap[i].ColumnType = "i";
			OutboundMap[i++].ColumnName = "Inbound";
			
			OutboundMap[i].ColumnNumber = i;      //H
			OutboundMap[i].ColumnType = "s";
			OutboundMap[i++].ColumnName = "SKILL_Id";
			
			
		}
	}
	public String convert(String data, Integer typeIdx) throws ParseException{
		String result = "";
		String type = OutboundMap[typeIdx].ColumnType;
		switch (type) {
		case "s":
			result = data ;
			if(data == null) {
				result = data = "";
			}
			if (data.contentEquals("null")) {
				result = "";
			}
			break;
		case "i":
			result = data;
			break;
		case "t2":
			if(data == null){
				result = "";
				break;
			}
			if (data.contentEquals("null")) {
				result = "";
				break;
			}
			if (data.contentEquals("")) {
				result = "";
				break;
			}
			SimpleDateFormat sdf = new SimpleDateFormat("M-d-yyyy h:mm:ss a");
			
			Date UTCDate = null;
			// String DATEFORMAT = "M-d-yyyy h:mm:ss a";
			String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";
			DateFormat format = new SimpleDateFormat(DATEFORMAT);
			try {
				UTCDate = format.parse(data);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			//  Have UTC Date - need to convert to local EST 
		    TimeZone tz = TimeZone.getTimeZone("US/Eastern");
		    Date LocalDate = new Date( UTCDate.getTime() + tz.getRawOffset() );

		    // if we are now in DST, increase by the delta.  Note that we are checking the local date, this is the KEY.
		    if ( tz.inDaylightTime( LocalDate )){
		        Date dstDate = new Date( LocalDate.getTime() + tz.getDSTSavings() );

		        // check to make sure we have not crossed back into standard time
		        // this happens when we are on the cusp of DST (7pm the day before the change for PDT)
		        if ( tz.inDaylightTime( dstDate )){
		        	LocalDate = dstDate;
		        }
		     }
		    // date is local time
			result =  sdf.format(LocalDate); 
		}
		return result;
	}

}
