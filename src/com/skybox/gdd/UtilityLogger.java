package com.skybox.gdd;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Filter;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import jakarta.activation.DataHandler;
import jakarta.activation.DataSource;
import jakarta.activation.FileDataSource;
import jakarta.mail.MessagingException;
import jakarta.mail.Multipart;
import jakarta.mail.NoSuchProviderException;
import jakarta.mail.SendFailedException;
import jakarta.mail.Session;
import jakarta.mail.internet.AddressException;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;

import com.sun.mail.smtp.SMTPTransport;


public class UtilityLogger {
	public static Logger LOGGER;
	public static String lastLogEntry = "";
	public static Integer LogCount = 0;
	
	public static void initLogger(String bu){
//		Handler consoleHandler = null;
		LOGGER = Logger.getLogger(UtilityLogger.class.getName());
		Handler fileHandler = null;
		Formatter simpleFormatter = null;
		try {
			
			// create simpleFormatter
			simpleFormatter = new SimpleFormatter();
			// configure default parent logger (console)
//			Logger parentLogger = LOGGER.getParent();
//			parentLogger.setLevel(Level.CONFIG); // Set to Fine to get more data
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String logDate = sdf.format(new Date());
			
			// create file handler
			fileHandler = new FileHandler("C:/RemoteCRMData/" + logDate + "GetDispDetail_" + bu +".log", true);
			// assign handler to logger object
			LOGGER.addHandler(fileHandler);
			
			
			
			fileHandler.setFilter(new Filter() {
				@Override
				public boolean isLoggable(LogRecord record){
					// return based on log settings
					boolean logit = false;
					if(record.getLevel().intValue() >= BU_Data.LogLevel.intValue()){
						logit = true;
						check4SevereProblems( record);
						if(record.getMessage().contains("Started")){
							if(LogCount > 0){
								LogCount--;
							}
						}
					}
					return logit;
				}
			}); 
			
			// setting formatter to handler
			fileHandler.setFormatter(simpleFormatter);
			
			//Set levels to handlers and Logger
//			fileHandler.setLevel(Level.CONFIG);   // Set to FINE for more detail logging
			LOGGER.setLevel(Level.ALL);
			
			LOGGER.fine("LOGGER Configuration done.");
			
			// console remove
//			LOGGER.removeHandler(consoleHandler);
			
		} catch (SecurityException e) {
			LOGGER.log(Level.SEVERE, "Error occur in Security",e);
			e.printStackTrace();
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Error occur in file Handler assignment",e);
			e.printStackTrace();
		}
	}
	private static void check4SevereProblems(LogRecord record){
		if(record.getLevel() == Level.SEVERE){
			//check is old report
			if(record.getMessage().contentEquals(lastLogEntry)){
				LogCount++;
				LogCount++;
				if(LogCount == 12){
					LogCount = 0;
					if(GetDispDetail.debug) return;
					// send email
					sendEmail(getMsgBody(record),"");
				}
			}else{
				if(GetDispDetail.debug) return;
				// new message
				sendEmail(getMsgBody(record),"");
				lastLogEntry = record.getMessage();
				LogCount = 0;
			}
		}
	}
	public static void sendEmail(String message, String fullPath){
        Properties props = System.getProperties();
        props.put("mail.smtps.host","smtp.gmail.com");
        props.put("mail.smtps.auth","true");
        Session session = Session.getInstance(props, null);
        MimeMessage msg = new MimeMessage(session);
        String addressTo = "cladewski@skyboxcommunications.com,mladewski@skyboxcommunications.com";
        try {
			msg.setFrom(new InternetAddress("AWSserver@skyboxcommunications.com"));;
			msg.setText(message);
			msg.setHeader("X-Mailer", "Server Mail");
			msg.setSentDate(new Date());
			
			if(!fullPath.isEmpty()){
				addressTo = BU_Data.emailAddress;
				msg.setRecipients(MimeMessage.RecipientType.TO, InternetAddress.parse(addressTo, false));
				String fileName = fullPath.substring(fullPath.indexOf("booking"));
				msg.setSubject("Dialer Dispositions for " + fileName.substring(fileName.indexOf("_") + 1, fileName.length()-4));
				
		        MimeBodyPart messageBodyPart = new MimeBodyPart();

		        Multipart multipart = new MimeMultipart();

		        messageBodyPart = new MimeBodyPart();
		        DataSource source = new FileDataSource(fullPath);
		        messageBodyPart.setDataHandler(new DataHandler(source));
		        messageBodyPart.setFileName(fileName);
		        multipart.addBodyPart(messageBodyPart);

		        msg.setContent(multipart);
			}else{
				// error email
				msg.setSubject("Server Error "+System.currentTimeMillis());
				msg.setRecipients(MimeMessage.RecipientType.TO, InternetAddress.parse(addressTo, false));
			}
			
			SMTPTransport t = (SMTPTransport)session.getTransport("smtps");
			t.connect("smtp.gmail.com", "cladewski@skyboxcommunications.com", "colleeN101");
			t.sendMessage(msg, msg.getAllRecipients());
//			System.out.println("Response: " + t.getLastServerResponse());
			t.close();
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (SendFailedException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}
	private static String getMsgBody(LogRecord record){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String msg = "";
		msg += "Time: " + sdf.format(new Date(record.getMillis())) + "\n";
		msg += "BU: " + BU_Data.buNumber + "\n";
		msg += "Msg: " + record.getMessage() + "\n";
		if(record.getThrown() != null){
			if(record.getThrown().getLocalizedMessage() != null){
				msg += "Thrown: " + record.getThrown().getLocalizedMessage() + "\n";
			}
			if(record.getThrown().getMessage() != null){
				msg += "Thrown: " + record.getThrown().getMessage() + "\n";
			}
			if(record.getThrown().getStackTrace() != null){
				StackTraceElement[] stack = record.getThrown().getStackTrace();
				for (StackTraceElement elem : stack){
					msg += "stack: " + elem.toString() + "\n";
				}
			}
		}
		
		return msg;
	}
}
