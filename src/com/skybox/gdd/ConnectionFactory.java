package com.skybox.gdd;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;



public class ConnectionFactory {
    //static reference to itself
    private static ConnectionFactory instance = new ConnectionFactory();
//    public static final String URL = "jdbc:mysql://skyboxcommunications.c8jkaslcdavw.us-east-1.rds.amazonaws.com:3306/Call_Center_Data";
    public static  String URL = BU_Data.DbURL + "?rewriteBatchedStatements=true";
    public static  String URL_SQL = BU_Data.DbURL;
//    public static final String USER = "lewdi4";
    public static  String USER = BU_Data.DbId;
//    public static final String PASSWORD = "volley2279";
    public static  String PASSWORD = ReadControlData.decript(BU_Data.DbPw);
    
//    public static  String DRIVER_CLASS = "com.mysql.jdbc.Driver"; 
    public static  String DRIVER_CLASS = "com.mysql.cj.jdbc.Driver"; 
    public static  String DRIVER_CLASS_SQL = "com.microsoft.sqlserver.jdbc.SQLServerDriver"; 
 
    //private constructor
    private ConnectionFactory() {
    	Logger LOG = UtilityLogger.LOGGER;
        try {
        	DRIVER_CLASS = "com.mysql.cj.jdbc.Driver";
        	DRIVER_CLASS_SQL = "com.microsoft.sqlserver.jdbc.SQLServerDriver"; 
        	if (BU_Data.DbURL.contains("database")) {
        		Class.forName(DRIVER_CLASS_SQL);
        	}else{
        		Class.forName(DRIVER_CLASS);
        	}
        } catch (ClassNotFoundException e) {
        	LOG.log(Level.SEVERE,"Cannot find DB Driver Class",e);
            e.printStackTrace();
        }
    }
     
    private Connection createConnection()throws SQLException{
        Connection connection = null;
        DRIVER_CLASS = "com.mysql.cj.jdbc.Driver";
        DRIVER_CLASS_SQL = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        URL = BU_Data.DbURL + "?rewriteBatchedStatements=true";
        URL_SQL = BU_Data.DbURL;
        USER = BU_Data.DbId;
        PASSWORD = ReadControlData.decript(BU_Data.DbPw);
    	try {
			if (BU_Data.DbURL.contains("database")) {
				Class.forName(DRIVER_CLASS_SQL);
			    connection = DriverManager.getConnection(URL_SQL, USER, PASSWORD);
			}else{
				Class.forName(DRIVER_CLASS);
			    connection = DriverManager.getConnection(URL, USER, PASSWORD);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
        return connection;
    }   
     
    public static Connection getConnection() throws SQLException {
        return instance.createConnection();
    }

}
